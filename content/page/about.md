---
title: About
description: 
date: '2021-09-06'
aliases:
  - about-me
  - contact
license: CC BY-NC-ND
lastmod: '2021-09-06'
menu:
    main: 
        weight: -90
        pre: user
---

I'm a high school student aiming for a career in electrical engineering or data science. Currently, I'm establishing a foundation of mathematics and computer science skills while exploring different paths to take in the future.

