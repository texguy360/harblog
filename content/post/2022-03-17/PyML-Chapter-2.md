# Chapter 2: Training Simple Machine Learning Algorithms for Classification

I've been reading a new machine learning book called [Machine Learning with PyTorch and Scikit-Learn](https://sebastianraschka.com/blog/2022/ml-pytorch-book.html) which contains conceptual explanations of different algorithms while implementing them with higher level ML libraries. It's focused on the conceptual but it has practical implementations that I can follow along as well as implement on my own. What I like is that it has so far put less emphasis on the actual coding but rather understanding the ideas. 

For this blog, I'll recap the first algorithm that the book presents while also implementing the algorithm on a dataset. The book uses the well-known [iris](https://en.wikipedia.org/wiki/Iris_flower_data_set) dataset, but I'll be using a similarly structured dataset called [palmerpenguins](https://allisonhorst.github.io/palmerpenguins/).

Before we start, I'd like to say that this came out lengthier than I thought it'd be because they are some neat concepts that are easy to graze over and not fully appreciate. I might have less descriptive posts as I transition into having more consistent updates on the blog.

## Perceptron Algorithm

The perceptron model is a classification algorithm that is based on a single neuron. In our brains, neurons only have **two states**: firing, or not firing. Therefore, an artificial neuron can function similarly by producing outputs of 0 or 1. This is the basic concept that our perceptron algorithm implements, which puts it in a group of algorithms called **binary classifiers**. Previously we used logistic regression, and despite being a regression with a continuous range of outputs, it is commonly used for binary classification since its range of outputs range from 0 to 1. 

### So how does it work?

We start with some function $h(z)$ that will take in the data and **decide the output** class given the inputs. This **decision function** is defined to be 

$$f(z)=\begin{cases}1,\quad z>0\\0,\quad z\leq0 \end{cases}$$

For those more familiar with [piecewise functions](https://en.wikipedia.org/wiki/Piecewise), one may recognize this as the [unit step function](https://en.wikipedia.org/wiki/Heaviside_step_function). 

![Decision Function](unitstep.png)

Now we must define $z$ to take on different values depending on the inputs. These inputs are also called **features**. A set of features make up a **training example**. These can be seen concretely in a later example with a dataframe.

Our features will be denoted as variables $x_1,x_2,x_3...$ or more compactly as the vector $\mathbf{x}$.

In my previous posts on linear regression and logistic regression, I used $x_0$ as a feature, which always had a value of $1$ and it'd be multiplied by a weight vector. This feature can also be called the **bias unit** as it helps adjust our $z$ value by a constant amount independent from the other features. We'll use the scalar $b$ to denote this constant term. 

Now we need the **weights**, which serve as as the backbone of carrying out the calculations to make the decision. These will be denoted as $w_1,w_2,w_3...$ or more compactly as the vector $\mathbf{w}$.

For the perceptron algorithm, these weights will simply scale the feature terms, and these terms will be added together. Mathematically, it looks like

$$z=w_1x_1+w_2x_2+w_3x_3+\cdots+b$$

Oh, and the bias unit is added as a sort of *y-intercept* while the rest of the terms determine the *slope*.

Using vectors (which speeds up computation time), we can compact this notation.

$$x=\begin{bmatrix}x_1&x_2&x_3&\cdots\end{bmatrix}$$
$$w=\begin{bmatrix}w_1&w_2&w_3&\cdots\end{bmatrix}$$

The sum of the product of the components of these vectors can be achieved through a dot product:

$$z=\mathbf{w}\cdot\mathbf{x}+b$$

This looks really similar to the equation of a line $y=mx+b$, and in practice it does the same thing! The function determines the equation of the boundary between two different classes, while our function $h(z)$ makes the decision. Therefore we can call $z$ our **decision boundary**.

![Decision Boundary](decisionboundary.png)

> As a note, usually the decision boundary is written using a vector-matrix product. Currently, the input features $\mathbf{x}$ only represents the features of one training example, whereas to account for more training examples, we need to include all $\mathbf{x}^{(i)}$. Therefore we use a matrix, with each row as a separate training example, and the columns representing the features.

$$\mathbf{x}=\begin{bmatrix}x_1^{(1)}&x_2^{(1)}&x_3^{(1)}&\cdots\\[1ex]x_1^{(2)}&x_2^{(2)}&x_3^{(2)}&\cdots\\[1ex]x_1^{(3)}&x_2^{(3)}&x_3^{(3)}&\cdots\\[1ex]\vdots&\vdots&\vdots&\ddots\end{bmatrix}$$

Then we multiply this with our weight vector, but to do this we'll need to take the transpose of this matrix to correctly multiply the components together. We'll then have

$$\mathbf{z}=\mathbf{w}\mathbf{x}^T+\mathbf{b}$$

Now this is a clean and compact equation in vectorized form so we can make faster computations. Faster algorithms and computations can sometimes be the make or break for choosing one over the other.

Since the weights $\mathbf{w}$ and the bias unit $\mathbf{b}$ determine the decision boundary, we need the perceptron algorithm to change the these values depending on whether they predicted the correct output or not. If the true class and the predicted class are the same, then we don't want to change our weights. If they are different however, we need to adjust them accordingly.

If the true class $y$ is $0$ and the predicted class $h(z)$ is $0$ (so $z\leq0$), then we will not change $w$.

If the true class $y$ is $0$ and the predicted class $h(z)$ is $1$ (so $z>0$), then $w$ must decrease so that $z$ will decrease.

If the true class $y$ is $1$ and the predicted class $h(z)$ is $0$ (so $z\leq0$), then $w$ must increase so that $z$ will increase.

If the true class $y$ is $1$ and the predicted class $h(z)$ is $1$ (so $z>0$), then we will not change $w$.

From this we can come up with one single equation that will adjust for all of these:

$$\Delta w=\alpha(y-h(z))x$$

- $\alpha$ is the learning rate, or the rate at which weights are updated
- $x$ is the feature corresponding to the given weight, so that the weight can be updated by the right scale

This process can be done with each weight and bias unit, and this change can then be added to the previous weight value to produce an updated weight value. Note that all of these weights are done **simultaneously** which means that the value of $z$ is not recomputed after each weight is updated. 

Every iteration the weights are updated is called an **epoch**, and this is iterated lots of times until it predicts all of the training examples correctly. 



For the perceptron model, there are some conditions that our classes must satisfy for the algorithm to work:

- The classes must be linearly separable (a straight line can separate the two classes)

- There must be a true class for each set of features under supervised learning.


Now let's implement it in Python code.


Import necessary libraries


```python
import pandas as pd # for dataframe manipulation
import numpy as np # for math and array manipulation
import matplotlib.pyplot as plt # for graphing fancy plots
```

Let's take a look at our data. We'll be using the [palmerpenguins](https://allisonhorst.github.io/palmerpenguins/) dataset, which is similar to the iris dataset that the book uses.


```python
penguins = pd.read_csv('../Datasets/palmerpenguins/penguins_size.csv') # read our csv data as a dataframe
penguins.head() # display the first few rows
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species</th>
      <th>island</th>
      <th>culmen_length_mm</th>
      <th>culmen_depth_mm</th>
      <th>flipper_length_mm</th>
      <th>body_mass_g</th>
      <th>sex</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.1</td>
      <td>18.7</td>
      <td>181.0</td>
      <td>3750.0</td>
      <td>MALE</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.5</td>
      <td>17.4</td>
      <td>186.0</td>
      <td>3800.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>40.3</td>
      <td>18.0</td>
      <td>195.0</td>
      <td>3250.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>36.7</td>
      <td>19.3</td>
      <td>193.0</td>
      <td>3450.0</td>
      <td>FEMALE</td>
    </tr>
  </tbody>
</table>
</div>



Each row represents an **observation**, and in machine learning terms they are also called **training examples**.

Each column represents a **variable**, and in machine learning terms they are also called **features**. However, since we will be classifying these penguins by species, the first column `species` will be our **output class**. 

For each training example, there are multiple columns that describe its features. We use these features in machine learning algorithms for classification which can then predict future observations. Since we know the species, or **true class** of each observation, we will be using a **supervised learning** algorithm. This will help us formulate a model to determine the **predicted class** for a set of features.


Just by looking at the first few training examples, we can see that there are some training examples that have NaN (Not a Number) values. We want to clean these up as they may affect our model so we'll use the `dropna()` function.


```python
peng = penguins.dropna()
peng.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species</th>
      <th>island</th>
      <th>culmen_length_mm</th>
      <th>culmen_depth_mm</th>
      <th>flipper_length_mm</th>
      <th>body_mass_g</th>
      <th>sex</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.1</td>
      <td>18.7</td>
      <td>181.0</td>
      <td>3750.0</td>
      <td>MALE</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.5</td>
      <td>17.4</td>
      <td>186.0</td>
      <td>3800.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>40.3</td>
      <td>18.0</td>
      <td>195.0</td>
      <td>3250.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>36.7</td>
      <td>19.3</td>
      <td>193.0</td>
      <td>3450.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.3</td>
      <td>20.6</td>
      <td>190.0</td>
      <td>3650.0</td>
      <td>MALE</td>
    </tr>
  </tbody>
</table>
</div>



Notice how after dropping the training examples that have NaN values, we are now missing those row indices which poses a problem for indexing later on. To fix this, we will use `reset_index()`. By default, this creates a new column with previous row indices, but that's unnecessary, so we'll add a `drop=True` parameter. 


```python
peng = peng.reset_index(drop=True)
peng.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species</th>
      <th>island</th>
      <th>culmen_length_mm</th>
      <th>culmen_depth_mm</th>
      <th>flipper_length_mm</th>
      <th>body_mass_g</th>
      <th>sex</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.1</td>
      <td>18.7</td>
      <td>181.0</td>
      <td>3750.0</td>
      <td>MALE</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.5</td>
      <td>17.4</td>
      <td>186.0</td>
      <td>3800.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>40.3</td>
      <td>18.0</td>
      <td>195.0</td>
      <td>3250.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>36.7</td>
      <td>19.3</td>
      <td>193.0</td>
      <td>3450.0</td>
      <td>FEMALE</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Adelie</td>
      <td>Torgersen</td>
      <td>39.3</td>
      <td>20.6</td>
      <td>190.0</td>
      <td>3650.0</td>
      <td>MALE</td>
    </tr>
  </tbody>
</table>
</div>




```python
print(peng['species'].unique()) # print the unique species
print(peng.columns) # print the column variable names
```

    ['Adelie' 'Chinstrap' 'Gentoo']
    Index(['species', 'island', 'culmen_length_mm', 'culmen_depth_mm',
           'flipper_length_mm', 'body_mass_g', 'sex'],
          dtype='object')
    

Our dataset contains three species of penguins, but since our perceptron algorithm can only have binary outputs, we'll have to choose two of these To start our classification, we need to find two features that can be used to classify our penguins. After looking at different graphs, I decided to compare `culmen_depth_mm` and `flipper_length_mm`. (The culmen depth is the distance between the top and bottom beak).


```python
pengy = peng.values # change our our dataframe to array form; easier for data graphing and calculations
print(pengy)
```

    [['Adelie' 'Torgersen' 39.1 ... 181.0 3750.0 'MALE']
     ['Adelie' 'Torgersen' 39.5 ... 186.0 3800.0 'FEMALE']
     ['Adelie' 'Torgersen' 40.3 ... 195.0 3250.0 'FEMALE']
     ...
     ['Gentoo' 'Biscoe' 50.4 ... 222.0 5750.0 'MALE']
     ['Gentoo' 'Biscoe' 45.2 ... 212.0 5200.0 'FEMALE']
     ['Gentoo' 'Biscoe' 49.9 ... 213.0 5400.0 'MALE']]
    

First, to better visualize our data, we'll use `matplotlib` to plot a scatter chart of all of the penguins with our two features on the axes.


```python
plt.scatter(pengy[pengy[:,0]=="Adelie"][:,3],
            pengy[pengy[:,0]=="Adelie"][:,4],
            color='red', marker='o', label='Adelie')
plt.scatter(pengy[pengy[:,0]=="Chinstrap"][:,3],
            pengy[pengy[:,0]=="Chinstrap"][:,4],
            color='orange', marker='s', label='Chinstrap')
plt.scatter(pengy[pengy[:,0]=="Gentoo"][:,3],
            pengy[pengy[:,0]=="Gentoo"][:,4],
            color='blue', marker='^', label='Gentoo')
plt.xlabel('Culmen Depth (mm)')
plt.ylabel('Flipper Length (mm)')
plt.legend(loc='upper left')
```




    <matplotlib.legend.Legend at 0x238906d6b20>




    
![png](output_14_1.png)
    


It looks like we're able to separate the Adelie and Gentoo classes using a line so let's regraph with only those data points on our graph.


```python
plt.scatter(pengy[pengy[:,0]=="Adelie"][:,3],
            pengy[pengy[:,0]=="Adelie"][:,4],
            color='red', marker='o', label='Adelie')
plt.scatter(pengy[pengy[:,0]=="Gentoo"][:,3],
            pengy[pengy[:,0]=="Gentoo"][:,4],
            color='blue', marker='^', label='Gentoo')
plt.xlabel('Culmen Depth (mm)')
plt.ylabel('Flipper Length (mm)')
plt.legend(loc='upper left')
```




    <matplotlib.legend.Legend at 0x238fc29caf0>




    
![png](output_16_1.png)
    


Now let's get started implementing the perceptron algorithm. Currently all of our data is in a **dataframe**, but we can pull the useful data out and place it into an **array** of values for easier mathematical computation. Since we're only focusing on the culmen depth and the flipper length as features, those will be the only values that will be taken. For our class, we'll need the values of the species. Since we're only looking at the Adelie and Gentoo species, we'll ignore all observations that contain `Chinstrap` as the species.


```python
pengX1 = peng[peng.species!='Chinstrap'][['culmen_depth_mm','flipper_length_mm']].values
pengY1 = peng[peng.species!='Chinstrap'].species.values
```

Currently the values in `pengY1` are string names of the species, but since our classes are in the form of numerical outputs 0 and 1, we'll need to transform those class names with their corresponding numerical class value. This can be done using `numpy`.


```python
ppnY = np.where(pengY1=='Adelie',1,0) # Adelie is classified as 1 and Gentoo is classified as 0
```

Now it's time to write our perceptron algorithm. I'll leave most of it unexplained as we've already seen the process of the perceptron, but I've left comments on what each line does. In addition to the process of the perceptron, we'll add a count for the number of errors, or incorrectly predicted training examples during each run.


```python
class Perceptron: # object oriented approach
    def __init__(self, lr=0.01, iters=50):
        self.lr = lr 
        self.iters = iters 
    
    def fit(self, Xs, y): # updates the weight

        self.weights = np.zeros(Xs.shape[1]) # creates weights
        self.biases = np.float_(0.)
        self.error_list = [] # list of errors for each iteration
        

        for _ in range(self.iters): # number of epochs/iterations through the dataset
            errors = 0 
            for xi, yi in zip(Xs, y): # for each training example
                update = self.lr * (yi - self.predict(xi)) # update is 0 if prediction is correct
                self.weights += update * xi # update the weights
                self.biases += update # update the biases
                errors += int(update != 0.0) # add one to the error when prediction is incorrect
            self.error_list.append(errors) # adds the number of errors to the 
        return self

    def hypothesis(self, X): # our function z
        return np.dot(X, self.weights) + self.biases # computes the hypothesis value used for predicting classes

    def predict(self, X): # our function h(z)
        return np.where(self.hypothesis(X) > 0.0, 1, 0) # predicts the class of the example



```

Let's use our perceptron on our data now. We'll need to create a perceptron object `ppn` and use it to fit weights onto our data. We'll also create a graph of each iteration and the number of incorrect predictions there were in that iteration. Note that the number of errors will ultimately converge to 0 since our classes are linearly separable.


```python
ppn = Perceptron(lr=0.05, iters=500) # learning rate of 0.05; run perceptron for 500 iterations

ppn.fit(pengX1,ppnY)

plt.plot(range(1,len(ppn.error_list) + 1),
         ppn.error_list, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Errors')


```




    Text(0, 0.5, 'Errors')




    
![png](output_24_1.png)
    


Finally, let's get some code to graph our decision boundary, and the classes that our perceptron will predict. (The code is not necessary to understand the concept for this part).


```python
from matplotlib.colors import ListedColormap
def plot_decision_regions(X, y, classifier, resolution=0.02):
    # setup marker generator and color map
    markers = ('^', 'o', 's' ,'v', '<')
    colors = ('blue', 'red', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                            np.arange(x2_min, x2_max, resolution))
    lab = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    lab = lab.reshape(xx1.shape)
    plt.contourf(xx1, xx2, lab, alpha = 0.3, cmap = cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
            y = X[y == cl, 1],
            alpha = 0.8,
            c = colors[idx],
            marker = markers[idx],
            label = f'Class {cl}',
            edgecolor = 'black')
```


```python
plot_decision_regions(pengX1, ppnY, classifier=ppn)
plt.xlabel('Culmen Depth (mm)')
plt.ylabel('Flipper Length (mm)')
plt.legend(loc='upper left')
plt.show()
```


    
![png](output_27_0.png)
    


And there we go, we've successfully used our perceptron for binary classification, and we can then use our weights for predicting classes of future penguins. 

In my next post (currently in progress and will come out within the next week) I'll include a similar algorithm ADALINE, use the functions in sklearn, expanding our perceptron to classify 3+ classes, and talk more about choosing good algorithms.

Some concluding thoughts:


```python
peng.mean()
```




    culmen_length_mm       43.994311
    culmen_depth_mm        17.160479
    flipper_length_mm     201.014970
    body_mass_g          4209.056886
    dtype: float64



- The culmen depth has an average value of around 17, while flipper length has an average value of around 200. There's an order magnitude of difference, but we use the same learning rate for both. How do we account for this?

- I chose classification with culmen depth and flipper length for the Adelie and the Gentoo species purposely because they were **linearly separable**. Is there a way to separate two classes when they are not linearly separable?

![Non-linear decision boundary](nonlinearlyseparable.png)

- How do we know that the algorithm will eventually converge to our desired decision boundary? (challenging)

