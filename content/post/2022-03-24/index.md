---
title: "Adaline and Basic Optimization"
date: 2022-03-24
lastmod: 2022-04-04
description: A new model with some optimization tips
categories: [
    Machine Learning
]
tags: [

]
draft: false
---

This time we'll talk about a new algorithm called Adaline which is similar to the perceptron model. We'll implement it and see some improvements that we can make.

From now on, I'll leave most code in expandable code blocks like so:
<details>
<summary><b>Greeting [CODE]</b></summary>

```python
# Greeting code
myGreeting = "Hello there"
print(myGreeting)

```
</details>

This'll condense the content to emphasize concepts but still have code available for understanding.

Onwards!



<script async src="https://unpkg.com/mermaid@8.14.0/dist/mermaid.min.js"></script>


## ADALINE (ADAptive LInear NEuron)

### Loss function

Here we have the loss function, which works exactly the same way as the cost function we previously saw. In this textbook, they use the term loss function rather than the term cost function but they can be used interchangeably. In a more advanced setting with more specificity in the usage of terms, the loss function is the cost of a single training example, while the cost function can be used to describe the **average loss** of the entire training dataset. (Many terms in ML are interchangeable and sometimes lead to some confusion -- at least it did for me -- so I'll start building up a glossary of terminology and their synonymous terms).

$$L(\mathbf{w},b)=\dfrac{1}{2m}\sum_{i=1}^m\left(y^{(i)}-h(z^{(i)})\right)^2$$

Like we've seen before, this loss function is modeled after the mean squared error and can be minimized using gradient descent. Just like how we did for linear regression, each iteration requires some computation to find some amount to update the weights and the bias. We'll need to take the derivative of the loss function to determine the direction and by how much we need to update our weight.

For each of our weights $w_j$, the derivative of the loss function is 

$$\begin{align*}\Delta w_j&=-\alpha \dfrac{\partial L}{\partial w_j}\newline 
&=-\alpha \dfrac{1}{m}\sum_{i=1}^m(y^{(i)}-h(z^{(i)}))x_j^{(i)}\end{align*}$$

Let's get deeper understanding of what this equation does:
- $y^{(i)}-h(z^{(i)})$ represents the standard error between the actual and predicted outputs
- $x_j$ represents the input corresponding to our weight. It does seem reasonable that how much we update the weight depends on the values we input
- $\alpha$ represents the learning rate, or some factor that we set to tune the rate of converging
- We're taking the average value of all our training examples, which is represented by $\Sigma$
- The negative sign changes the weight to reach a minimum. i.e. if we're facing uphill, we want to go the opposite direction to go downhill to a minimum.

More generally, the **gradient** is a vector, or a condensed way of representing the derivatives of all the weights $w_j$. It is commonplace to use the $\nabla$ (nabla) symbol, where $\nabla_xf$ represents the gradient of $f$ with respect to the $x$. In our situation we can rewrite the update of all our weights as

$$\Delta w = -\alpha\nabla_\mathbf{w}L$$

The updates to our bias unit can be completed the same way.

Now let's take a look at how this compares to our **perceptron** model. Below I have a flowchart of both the perceptron and Adaline models. The models start with a set of inputs and which are then used to compute the weights and a net input. 

### Perceptron
<div class="mermaid">
    flowchart LR;
    subgraph Input
        f1((feature 1)) --> w1((weight 1))
        f2((feature 2)) --> w2((weight 2))
        fj((feature j)) --> wj((weight j))
        b(bias) & w1 & w2 & wj --> net("#Sigma; net input \n function")
    end
    subgraph Error
        direction RL
        err[Errors] --> a(["Weights & bias updates \n (gradient descent)"])
    end
    y[y - True class] --> err
    net --> thr(("threshold \n function"))
    a --> Input
    thr --> yhat{"y&#770;" - Predicted class} --> err
    style y fill:#17aa3d,stroke:#3d873d,stroke-width:4px
    style yhat fill:#42e66b,stroke:#31bf51,stroke-width:4px
    style f1 fill:#42a0ff,stroke:#354adf,stroke-width:4px
    style f2 fill:#42a0ff,stroke:#354adf,stroke-width:4px
    style fj fill:#42a0ff,stroke:#354adf,stroke-width:4px
</div>

For the perceptron, this net input can be used directly in a threshold function, which we saw to be our unit step function, which would determine the predicted class. If there are still errors between the predicted class and the true class, the perceptron will continue to update the weights using the new predicted class. 

### Adaline

<div class="mermaid">
    flowchart LR;
    subgraph Input
        f1((feature 1)) --> w1((weight 1))
        f2((feature 2)) --> w2((weight 2))
        fj((feature j)) --> wj((weight j))
        b(bias) & w1 & w2 & wj --> net("#Sigma; net input \n function")
    end
    subgraph Error
        direction RL
        err[Errors] --> a(["Weights & bias updates \n (gradient descent)"])
    end
    y[y - True class] --> err
    net --> act("activation \n function") --> err & thr(("threshold \n function"))
    a --> Input
    thr --> yhat{"y&#770;" - Predicted class}
    style y fill:#17aa3d,stroke:#3d873d,stroke-width:4px
    style yhat fill:#42e66b,stroke:#31bf51,stroke-width:4px
    style f1 fill:#42a0ff,stroke:#354adf,stroke-width:4px
    style f2 fill:#42a0ff,stroke:#354adf,stroke-width:4px
    style fj fill:#42a0ff,stroke:#354adf,stroke-width:4px
    style act fill:#cd7474,stroke:#cc4f48,stroke-width:4px
</div>

For Adaline, the net input is used in an activation function which will then have its errors computed to update the weights. This is used to calculate a new net input and this process repeats until the weights converge.

Before we get into more details, let's start implementing Adaline to get a better understanding of it all.

<details>
<summary><b>Adaline Gradient Descent [CODE]</b></summary>

```python
class AdalineGD:
    def __init__(self, lr=0.01, iters=50):
        self.lr = lr
        self.iters = iters

    def fit(self, Xs, y):

        self.weights = np.zeros(Xs.shape[1])
        self.biases = np.float_(0.)
        self.losses = []

        for _ in range(self.iters):
            net_input = self.z_input(Xs) # calculate the net input z
            output = self.activation(net_input) # utilize the activation function (for Adaline it's just the identity) on the net input
            stderrs = y - output # calculate the standard errors from this activation function
            self.weights += self.lr * Xs.T.dot(stderrs) / Xs.shape[0] # calculate the updates to the weights using the errors
            self.biases += self.lr * stderrs.mean() # calculate the updates 
            loss = (stderrs**2).mean() # calculate the loss from errors
            self.losses.append(loss) # update the history of losses

        return self



    def z_input(self, X): # compute our net input from our inputs
        return np.dot(X, self.weights) + self.biases

    def activation(self, X): # h(z)
        # practically does nothing as it returns the identity, but can be changed for other algorithms
        return X

    def predict(self, X): # our threshold function
        return np.where(self.activation(self.z_input(X)) >= 0.5, 1, 0)

        
```

</details>

Below we can see the importance of choosing a good learning rate. On the left we have a learning rate that is **too large** which will make the loss from the weights increase because the updates will always overshoot. On the right we have a learning rate that is **too small** which requires a lot more iterations before the loss can converge to a minimum.

<details>
<summary><b>Graph [CODE]</b></summary>

```python
fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))
ada1 = AdalineGD(iters=20, lr=0.001).fit(pengX1, pengY1)
ax[0].plot(range(1, len(ada1.losses) + 1),
np.log(ada1.losses), marker='o')
ax[0].set_xlabel('Epochs')
ax[0].set_ylabel('log(Mean squared error)')
ax[0].set_title('Adaline - Learning rate 0.1')
ada2 = AdalineGD(iters=20, lr=0.0000001).fit(pengX1,pengY1)
ax[1].plot(range(1, len(ada2.losses) + 1),
ada2.losses, marker='o')
ax[1].set_xlabel('Epochs')
ax[1].set_ylabel('Mean squared error')
ax[1].set_title('Adaline - Learning rate 0.0000001')
plt.show()
```

</details>
    
![Learning Rates](learning-rate.png)
    


Once we've chosen a good learning rate, we can see that the loss decreases and quickly converges to a minimum.

<details>
<summary><b>Graph [CODE]</b></summary>

```python
ada3 = AdalineGD(lr=0.00001, iters=30).fit(pengX1, pengY1)
plt.plot(range(1,len(ada3.losses)+1), ada3.losses, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.show()
```

</details>
    
![Loss for each epoch](epoch-adagd.png)
    


Wait... If we have a linearly separable set of data why does the loss converge at a value considerably higher than 0? Let's graph the decision boundary and regions.

<details>
<summary><b>Graph [CODE]</b></summary>

```python
plot_decision_regions(pengX1, pengY1, classifier=ada3)
plt.title('Adaline - Gradient descent')
plt.xlabel('Culmen Depth (mm)')
plt.ylabel('Flipper Length (mm)')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()
```

</details>
    
![Bad decision boundary](decision-bad.png)
    


That doesn't seem right. What we need to notice is that the two features we're comparing are on different scales, namely, the culmen depth has an average value of around 17 while the flipper length has an average value of around 200. That's around an order of magnitude difference, and the spread of these values around the mean value is quite low. 

When they're on different scales it makes it difficult and sometimes impossible for our decision boundary to converge at the actual minimum. 

### Standardizing Features

To converge with a desired decision boundary, we'll need to standardize our values. This is done by computing the z-scores for each of our features. The z-score for the $j$ th feature in our dataset is calculated using the equation 

$$z_j=\dfrac{x_j-\bar{x}_j}{S_j}$$

where,
- $x_j$ is the $j$ th feature
- $\bar{x}_j$ is the mean of the $j$ th features
- $S_j$ is the sample standard deviation of the $j$ th features

After modeling more data, we'll see that standardizing our variables will also allow our model to converge faster.

Let's now standardize our features and rerun the algorithm.

<details>
<summary><b>Standardization [CODE]</b></summary>

```python
pengX1_std = np.copy(pengX1) # makes a copy of our features for standardizing

pengX1_std[:,0] = (pengX1[:,0] - pengX1[:,0].mean()) / pengX1[:,0].std() # standardizes our features using the equation for z-score
pengX1_std[:,1] = (pengX1[:,1] - pengX1[:,1].mean()) / pengX1[:,1].std()
```

</details>

<details>
<summary><b>Graph [CODE]</b></summary>

```python
adaGD = AdalineGD(lr=0.1, iters=50)
adaGD_fit = adaGD.fit(pengX1_std, pengY1)
plt.plot(range(1,len(adaGD_fit.losses)+1), adaGD_fit.losses, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.show()
```
</details>

    
![Loss for each epoch (standardized features)](epochs-std.png)
    

<details>
<summary><b>Graph [CODE]</b></summary>

```python
plot_decision_regions(pengX1_std, pengY1, classifier=adaGD)
plt.title('Adaline - Gradient descent')
plt.xlabel('Culmen Depth (standardized)')
plt.ylabel('Flipper Length (standardized)')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()
```

</details>

    
![Good decision boundary (standardized features)](decision-good.png)
    


This looks much better. Since the classes are linearly separable with the features we chose we see that our decision boundary cleanly separates our classes allowing us to predict every training example correctly. As I mentioned earlier, since the Adaline function calculates the updates based based on a standard error between a net input value and the true class, rather than a predicted class and the true class (as is true for the perceptron), this allows our algorithm to converge to a decision boundary even when our classes are **not linearly separable**. Since we also know how to standardize our variables, let's also utilize that in the process.

<details>
<summary><b>Graph [Code]</b></summary>

```python
plt.scatter(pengy[pengy[:,0]=="Adelie"][:,2],
            pengy[pengy[:,0]=="Adelie"][:,5],
            color='red', marker='o', label='Adelie')
plt.scatter(pengy[pengy[:,0]=="Gentoo"][:,2],
            pengy[pengy[:,0]=="Gentoo"][:,5],
            color='blue', marker='^', label='Gentoo')
plt.xlabel('Culmen Length (mm)')
plt.ylabel('Body Mass (g)')
plt.legend(loc='upper left')
```

</details>




    
![Culmen Length vs. Body Mass (contains overlap)](overlap.png)
    


We can now see that the Adelie and Gentoo classes have some overlap when we use the culmen length and body mass features. However, there is still definitely some boundary between the two regions that give us a good measure of predicting the class. Let's standardize our features, fit the Adaline model to our data, and graph our resulting decision boundary.

<details>
<summary><b>Fit Model [CODE]</b></summary>

```python
pengX2 = peng[peng.species!='Chinstrap'][['culmen_length_mm','body_mass_g']].values
pengX2_std = np.copy(pengX2)
pengX2_std[:,0] = (pengX2[:,0] - pengX2[:,0].mean()) / pengX2[:,0].std()
pengX2_std[:,1] = (pengX2[:,1] - pengX2[:,1].mean()) / pengX2[:,1].std()

adaGD2 = AdalineGD(lr=0.1, iters=50)
adaGD2.fit(pengX2_std,pengY1)
```

</details>



<details>
<summary><b>Graph [CODE]</b></summary>

```python
plot_decision_regions(pengX2_std, pengY1, classifier=adaGD2)
plt.title('Adaline - Gradient descent')
plt.xlabel('Culmen Length (standardized)')
plt.ylabel('Body Mass (standardized)')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()
```

</details>

    
![Adaline - Gradient Descent](adagd-decision.png)
    


Not bad at all. This decision boundary separates the two classes pretty well.

## Optimizing Gradient Descent

We've seen that standardizing our features allows us to converge at a lower minimum, as well as converge at a faster rate. Are there any other methods of speeding up the computation, especially in cases where we have several magnitudes times more quantities of data?

The Adaline model using gradient descent updates the weights after considering all the training examples. More specifically, this is called **full batch gradient descent** because all of the training examples are accounted for. To decrease computation time, instead of taking into account all of the training examples, we can **sample** a smaller amount of our data. This can be called **mini-batch gradient descent** and we'll I'll show some implementations of this in later posts, however the concept is the same to that of full batch gradient descent, but we are now randomly sampling a portion of the training examples for faster computation each iteration. 

Of course, this will in turn sacrifice the precision of the calculated weights because we are no longer taking into account all of the training examples simultaneously. That's why if we graph the losses for each epoch, we see that the losses will fluctuate around the minimum.

Now if we start doing that to the extreme where we sample only one training example at a time, we get something similar to **stochastic gradient descent**.

### Stochastic Gradient Descent

A neat property of our perceptron model is that it updates the weights after each training example which means the computation for each iteration is minimal. We can use something similar with gradient descent to achieve faster computation with consistent accuracy.

The word **stochastic** just means that each of our weights is randomly selected. However, each iteration of our gradient descent will still involve every training example. To guarantee this, we can randomly **permute** our training examples and compute our weights for each update. 

Like with many models, faster computation usually suggests less precise models in the long run. Since we're updating the weights after considering individual training examples, our weights will fluctuate more. To help with this, we can have an **adaptive learning rate** which shrinks as we iterate more and more to help with eventually converging to a more stable value. This doesn't change our computation time, but helps make our model more precise. An example equation for how an adaptive learning rate could look like:

$$\alpha = \dfrac{c_1}{n + c_2}$$

Where $c_1$ and $c_2$ are constants and $n$ is the $n$th iteration that the model's been running for.

Boom! Now let's edit our `AdalineGD` class to utilize stochastic gradient descent and implement it.


<details>
<summary><b>Adaline Stochastic Gradient Descent [CODE]</b></summary>

```python
class AdalineSGD:
    def __init__(self, lr=0.01, iters=50, shuffle=True):
        self.lr = lr
        self.iters = iters
        self.shuffle = shuffle
        self.w_inited = False # whether or not weights have been changed or not. here for readability and future implementations
        self.hist_losses = []

    def fit(self, Xs, y):
        if not self.w_inited:
            self._init_weights(Xs.shape[1])
        for _ in range(self.iters): # for each iteration
            if self.shuffle:
                Xs, y = self._shuffle(Xs, y) # shuffle our training examples
            losses = [] 
            for xi, yi, in zip(Xs, y): 
                losses.append(self._update_weights(xi, yi)) # update the weights for each training example and add its loss
            self.hist_losses.append(np.mean(losses)) # take the average loss
        return self

    def _shuffle(self, Xs, y): # permutes our training examples in a random order
        idx = np.random.permutation(len(y)) 
        return Xs[idx], y[idx] 

    def _init_weights(self, m): # init weights. can be changed to init with random weights rather than zeros
        self.weights = np.zeros(m)
        self.biases = np.float(0.)
        self.w_inited = True

    def _update_weights(self, xi, yi): 
        output = self.activation(self.net_input(xi))
        error = yi - output
        self.weights += self.lr * xi * (error)
        self.biases += self.lr * error
        loss = error**2
        return loss # returns the loss from the training example 

    def net_input(self, X): # compute our net input (z) from our inputs
        return np.dot(X, self.weights) + self.biases

    def activation(self, X):
        # practically does nothing as it returns the identity, but can be changed for other algorithms
        return X

    def predict(self, X): # threshold function
        return np.where(self.activation(self.net_input(X)) >= 0.5, 1, 0)


```

</details>

Let's fit our model onto the culmen length vs. body mass features. We'll graph the results as well as the losses.

<details>
<summary><b>Fit and Graph [CODE]</b></summary>

```python
ada_sgd = AdalineSGD(iters=15, lr=0.01)
ada_sgd.fit(pengX2_std, pengY1)
plot_decision_regions(pengX2_std, pengY1, classifier=ada_sgd)
plt.title('Adaline - Stochastic gradient descent')
plt.xlabel('Culmen Length (standardized')
plt.ylabel('Body Mass (standardized')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()
plt.plot(range(1, len(ada_sgd.hist_losses) + 1), ada_sgd.hist_losses,marker='o')
plt.xlabel('Epochs')
plt.ylabel('Average loss')
plt.tight_layout()
plt.show()
```

</details>
    
![Adaline - Stochastic Gradient Descent](adasgd-decision.png)
    



    
![Loss for each epoch (SGD and standardized features)](epochs-sgd.png)
    


Wow, not only does our decision boundary separate the classes well, it also converges in only 2 iterations. 

Now that we've seen two different types of models and we've described some techniques to optimize our model, we're going to start looking into using libraries that have adapted these algorithms in the next posts.

To end it off, here are some concluding thoughts and questions to be think about and answer later.

## Concluding Thoughts

- How can we classify more than two classes simultaneously?

- Let's suppose for our linearly separable set of features, we didn't note the classes. So our graph would like:

<details>
<summary><b>Graph [CODE]</b></summary>

```python
plt.scatter(pengy[pengy[:,0]!="Chinstrap"][:,3],
            pengy[pengy[:,0]!="Chinstrap"][:,4],
            color='black', marker='o', label='???')
plt.xlabel('Culmen Depth (mm)')
plt.ylabel('Flipper Length (mm)')
plt.legend(loc='upper left')
```
</details>

    
![png](no-class.png)
    


From this, a human can still that there are clearly two separate groups. In other words, is it possible to classify these two groups without given the true class? 
