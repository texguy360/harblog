---
title: "Rebuilding the WY Robotics Website"
date: 2021-10-04
lastmod: 2021-10-21
description: Making the robotics website more customizable and useful (hopefully)
categories: [
    Robotics
]
tags: [
    club,
    website
]
draft: false
---

The robotics club is up and running, and to facilitate some better documentation of our progression throughout this upcoming season, I've decided to revamp our website. Initially hosted on Google sites at https://sites.google.com/view/wyrobotics but I thought it'd be more customizable to build our website using Hugo due to it's ability to handle markdown and other shortcode templates. This would allow for a a smooth compatability of engineering notebook entries written in Google docs to a website.

Entries currently on the Google docs can be easily converted to markdown files and listed onto the website as a series of blog posts. This will be more accessible for members and others to view. Additionally, markdown files are easily converted into pdf form.

The new website, found at https://wyrobotics.netlify.app is built with the Hugo framework (with Wowchemy theme), hosted on GitHub, and deployed using the Netlify web platform. 

Soon, we'll find a new domain as its home. 

UPDATE 10/21/2021:
https://www.wyrobotics.org is the website!