---
title: "A Semester in Robotics"
date: 2022-02-01
lastmod: 2022-02-03
description: 
categories: [
    Robotics
]
tags: [

]
draft: false
---

Over the first semester, progress for implementing machine learning algorithms has been slow. I’ve read texts, watched lectures, and have replicated the algorithms I’ve learned in Python, but I have yet to complete a full analysis on a dataset using those tools. In my senior experience, I’ve primarily been using my time to design and build for robotics, and since the competitive season is approaching an end within the next month or two, I’d like to run through the thought process and revisions behind the various parts of our robot. There have been more prototypes for our robot this year compared to previous years which has held back our ability to compete with the most polished robot, but it has ultimately allowed us to understand the advantages and flaws of different mechanisms. Below, I’ll describe our engineering process, and the observations that propelled us to make certain changes. 

## Game Strategy
Our overall choice for the design of the robot was based on our game strategy for this year's game. Below is a diagram of the field for the Freight Frenzy season, with the warehouses containing freight on the top corners of the field, the alliance shipping hubs located 4 feet away from each of the left and right sides, and a shared shipping hub at the top of the field, sectioned off by pipe barriers on the ground. 

![Freight Frenzy Field](thefield.png)

Throughout the season, the biggest distinction among robots is the size of the robot. Particularly, there are *skinny* robots that fit through the 13.75" gaps, and there are larger regular-sized robots that use the full build size, but are then required to drive over the barriers. 


We outlined the robot to take on the following structure, with certain considerations:
1.  Skinny robot
    - more optimized for movement through the gap
    - can drive over the barrier in emergencies
    - uses mecanum wheels to allow for strafing (omnidirectional movement)
2. Extender
   - allows us to travel in a straight line across the field wall, while extending to place
   - ability to place in shared shipping hub faster
   - will pose weight distribution issues
3. Dual-sided intake
   - required so that extender can always face the inside of the field
   - must intake one mineral only

With these general ideas, our pathing for matches would consist of driving in straight lines to run cycles on the either shipping hubs. 

Cycle Sequence:
1. Drive along wall to warehouse
2. Intake freight
3. Grab freight
4. Drive along wall to shipping hub
5. Extend while driving
6. Stop and drop freight
7. Repeat

![Cycle Pathing](movement.png)

Finally, in endgame we'll drive to the carousel and spin all the ducks off. 

## Extender

This is the mechanism that I worked on tuning the most. Early in the year, I spent time testing different types of slides to find which worked the best.


## Rotator and Grabber



## Intake

The intake underwent several different edits. There were two large versions of the intakee: one with the 




