# Chapter 3: A Tour of Machine Learning Classifiers Using Scikit-Learn


```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
```


```python
from matplotlib.colors import ListedColormap
def plot_decision_regions(X, y, classifier, classes=None, tests=None, resolution=0.02):
    # setup marker generator and color map
    markers = ('o', 's', '^' ,'v', '<')
    colors = ('red', 'orange', 'blue', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                            np.arange(x2_min, x2_max, resolution))
    lab = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    lab = lab.reshape(xx1.shape)
    plt.contourf(xx1, xx2, lab, alpha = 0.1, cmap = cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot class examples

    if tests:
        alp = 0.3
    else:
        alp = 1

    for idx, cl in enumerate(np.unique(y)):
        if classes:
            label = classes[cl]
        else: 
            label = f'Class {cl}'
        plt.scatter(
            x = X[y == cl, 0],
            y = X[y == cl, 1],
            alpha = alp,
            c = colors[idx],
            marker = markers[idx],
            label = label)
    if tests:
        for idx, cl in enumerate(np.unique(y)):
            xs = X[tests]
            ys = y[tests]
            plt.scatter(
                x = xs[ys == cl, 0],
                y = xs[ys == cl, 1],
                alpha = 0.8,
                c = colors[idx],
                marker = markers[idx],
                edgecolor = 'black')

```


```python
penguins = pd.read_csv('../Datasets/palmerpenguins/penguins_size.csv')
peng = penguins.dropna().reset_index(drop=True)
pengs = peng.values
species = {'Adelie': 0, 'Chinstrap': 1, 'Gentoo': 2}
pengY = peng.species.values
pengY = [species[p] for p in pengY]
pengX1 = peng[['culmen_length_mm','flipper_length_mm']].values

```

Since we're using the sophisticated machine learning library `sklearn`, we'll be able to classify multiple classes as well ones that are not linearly separable. Therefore, we'll be classifying all three classes of penguins by using the `culmen_length` and `flipper_length` features.


```python
plt.scatter(pengs[pengs[:,0]=="Adelie"][:,2],
            pengs[pengs[:,0]=="Adelie"][:,4],
            color='red', marker='o', label='Adelie')
plt.scatter(pengs[pengs[:,0]=="Chinstrap"][:,2],
            pengs[pengs[:,0]=="Chinstrap"][:,4],
            color='orange', marker='s', label='Chinstrap')
plt.scatter(pengs[pengs[:,0]=="Gentoo"][:,2],
            pengs[pengs[:,0]=="Gentoo"][:,4],
            color='blue', marker='^', label='Gentoo')
plt.xlabel('Culmen Length (mm)')
plt.ylabel('Flipper Length (mm)')
plt.legend(loc='upper left')
```




    <matplotlib.legend.Legend at 0x201a87abf40>




    
![png](output_5_1.png)
    


Now let's implement a couple algorithms using the optimized sklearn implementations. 

## Perceptron with Scikit-learn

The reason why we approached our previous implementations using an object-oriented approach is because sklearn consists of various models, each having a class with various methods. 

Before we create our perceptron object, we'll first split our dataset into features and class labels. Furthermore, we'll split our data into training and tests sets. The training set will be used to fit our model while the test set will be used to evaluate our model's success.


```python
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(pengX1, pengY, test_size=0.3, random_state=1, stratify=pengY)
```

Using the `train_test_split` function we can split our `pengX1` features into training and test examples, and our `pengY` class labels into training and test examples. The `test_size` parameter allows us to control the proportion of the data to be used for testing. The `random_state` parameter allows us to set a random seed state when randomly splitting the data so that we can reproduce any observations later. Finally, we have the `stratify` parameter which takes in the class labels so that the proportion of test examples for each individual class are each as close to our desired `test_size` as possible.  


```python
# stratify
print(
    np.bincount(pengY),"\n", # number of examples for each class in our entire dataset
    np.bincount(y_train),"\n", # number of examples for each class in the training dataset
    np.bincount(y_train)/np.bincount(pengY) # proportion of examples used for training in each class
)
```

    [146  68 120] 
     [102  47  84] 
     [0.69863014 0.69117647 0.7       ]
    

To further setup our data for use in our models, we'll need to standardize our data. Luckily, scikit-learn also offers that functionality so all we'll need is to use the `StandardScaler` object. 


```python
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)
```

Now that we have our standardized training and test sets, we can now create our `Perceptron` model. The `eta0` parameter in the following code represents the learning rate. Finally, with our perceptron, we can use the `fit()` function to fit our model to our training data. 


```python
from sklearn.linear_model import Perceptron
ppn = Perceptron(eta0 = 0.1, random_state=1)
ppn.fit(X_train_std, y_train)
```




    Perceptron(eta0=0.1, random_state=1)



With our model trained, we can now predict classes for each set of features in our test set. We can compare this array of predicted classes with the true classes of the test class to see the number of poorly classified test examples.


```python
y_pred = ppn.predict(X_test_std)
print('Misclassified examples: %d' % (y_test != y_pred).sum())
```

    Misclassified examples: 2
    

Furthermore, we can use some of scikit-learn's error evaluation functions to score our model. This is simply taking the percent of accurate predictions. 


```python
from sklearn.metrics import accuracy_score
print('Accuracy: %.3f' % accuracy_score(y_test, y_pred)) # score given the predicted and true class labels
print('Accuracy: %.3f' % ppn.score(X_test_std, y_test)) # score with our ppn model given test set
```

    Accuracy: 0.980
    Accuracy: 0.980
    

Lastly we can look at our model with the decision boundaries separating our three classes. The bolded markers represent the examples used for testing while the lightly shaded markers represent the training examples. 


```python
X_combined_std = np.vstack((X_train_std, X_test_std))
y_combined = np.hstack((y_train, y_test))
tests=range(len(y_train),len(y_combined))
plot_decision_regions(
    X=X_combined_std,
    y=y_combined,
    classifier=ppn,
    classes=['Adelie','Chinstrap','Gentoo'],
    tests=tests)
plt.xlabel('Culmen Length (standardized)')
plt.ylabel('Flipper Length (standardized)')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()
```


    
![png](output_21_0.png)
    


## Implementing Logistic Regression

Let's recall what logistic regression entails. Despite its name, it's an algorithm that *classifies* data by using the sigmoid function:

$$S(z)=\dfrac{1}{1+e^{-z}}$$

This sigmoid function takes in a **net input** of $z$ and computes a value that lies between $0$ and $1$, which is finally used in a threshold function to determine the predicted class. 

As you may or may not have noticed, logistic regression looks similar to the Adaline model we used previously, and that is because there's only one major difference: the activation function. The **activation function** of our Adaline model is an identity function $\phi(x)=x$ (it is commonplace to use the greek letter $\phi$ phi for the activation function), while the activation function for our logistic regression is the sigmoid function $\phi(x)=\dfrac{1}{1+e^{-x}}$. A change in the activation function will require us to change the loss function so that the equations for the updates remain the same, however we only need to know the loss function if we want to keep a record of the history of losses.  The math and conceptual understanding behind this can be found in my previous post on logistic regression, but this time I'll be using `sklearn` to implement this function. 


```python
from sklearn.linear_model import LogisticRegression
logReg = LogisticRegression(
    C=100.0, # regularization scaler
    solver='lbfgs', # optimization algorithm
    multi_class='ovr' # one vs. rest
)
logReg.fit(X_train_std, y_train)
plot_decision_regions(
    X_combined_std,
    y_combined,
    classifier=logReg,
    tests=tests
)
plt.xlabel('Culmen Length (standardized)')
plt.ylabel('Flipper Length (standardized)')
plt.show()
```


    
![png](output_23_0.png)
    


When creating our `LogisticRegression` object, we can see that we need to define a regularization term, an optimization algorithm, and the method for multiclass classification. The optimization algorithm we used in our logistic regression implementation was gradient descent, but there are other methods which are faster for our computer to compute. Since we have three classes of penguins, we'll need to use a multiclass classification method. The one that's chosen is `ovr` or **one v. rest** where we choose one class to fit under a certain label, while the rest of the classes are fit under a different label. Since there are 2 labels, one can use binary classification, to determine when certain examples fit in a class. This can be done for each of the classes to determine all the decision boundaries. Finally, we have the regularization term, which helps regulate the fit of the decision boundaries which we'll look into more later. 

First let's procede by listing the probabilities that are calculated from the model for a few test examples.


```python
print(
    logReg.predict_proba(X_test_std[:5]),'\n',
    logReg.predict_proba(X_test_std[:5]).argmax(axis=1)
)
```

    [[5.93328974e-01 4.06670866e-01 1.59794411e-07]
     [2.43058637e-01 7.56729627e-01 2.11736747e-04]
     [4.07159306e-03 1.21571344e-01 8.74357063e-01]
     [9.91578139e-01 8.41113929e-03 1.07213428e-05]
     [9.99714921e-01 2.50158094e-04 3.49207329e-05]] 
     [0 1 2 0 0]
    

From this we can see that each test example has a certain probability attached to each of the possible classes. The predicted class for each test example is simply the class that has the largest probability. Therefore we can use the `argmax()` function to find the index of the largest probability which represents the class. This use of `predict_proba()` and `argmax()` is synonymous to using the function `predict()`. Knowing this, let's utilize this function and print the result and compare it to the true class labels for those 5 test examples. 


```python
print(
    y_test[:5],
    logReg.predict(X_test_std[:5])
)
```

    [1, 1, 2, 0, 0] [0 1 2 0 0]
    

This is interesting. The first test example's true class is 1 but it's been predicted to be 0. This suggests that the first test example is misclassified by our model. Let's take a look at the number of misclassified examples in our entire test set, as well as the proportion of the test set that is misclassified.


```python
ypred = logReg.predict(X_test_std)
misclassified = (ypred != y_test).sum()
ntest = len(ypred)
print(
    misclassified,'\n',
    '{:.2f}%'.format(misclassified/ntest*100)
)
```

    3 
     2.97%
    

Considering all our examples in our test dataset, we only have 3 misclassified examples which gives around a 2.97% misclassification rate. All in all, this provides us with a pretty accurate model for predicting classes. 

### Fitting



When we fit our model, depending on the errors, we might have a complex model, or a simple model. Let's see an example of what this looks like given a dataset. Let's create a dataset to be used to train our model.


```python
myX = np.random.normal(0,3,100)
myE1 = np.random.normal(12,7,60)
myE2 = np.random.normal(7,3,40)
myY1 = myX[0:60]**2 + myE1
myY2 = myX[60:100]**2 - myE2
myY = np.concatenate((myY1,myY2))
myClasses = np.concatenate((np.zeros(60),np.ones(40)))
myFeatures = np.stack((myX,myY), axis=1)
```


```python
plt.scatter(x=myX[0:60],y=myY[0:60],color='r')
plt.scatter(x=myX[60:100],y=myY[60:100],color='b')
```




    <matplotlib.collections.PathCollection at 0x201af024760>




    
![png](output_34_1.png)
    


![Some Artificial Data](fitting.png)

Now given this model, there are several different ways we can fit our model. Suppose we were forced to use a simple graph of a straight line. The best we can do to mark the decision boundary is to have a horizontal line to separate some of the red markers and some of the blue markers. 

If our model could be unrestricted, then our model could be trained to fit every single marker correctly as shown in the graph on the right. However, problems arise when trying to use this model on test data. 

![Fitting](fits.png)

A common issue when choosing our model is to find one that can fit well from the training set and perform well on the test set. The issue of **underfitting** comes from a high bias, meaning that we've placed a lot of restrictions as to how complex our model can be. On the other end of the spectrum is the problem of **overfitting** which comes from high variance. High variance comes from an overly complex model that has attempted to fit every single training example, but when predicting classes on a test set, the model produces a high variance. 

As shown in the examples above, our set of data can be split with a quadratic curve but there remains a few misclassified examples. Since the model is less complex, it has a greater chance of optimally representing the broader pattern which is applicable to the test examples. The left graph represents a horizontal line that underfits due to its simplicity, and the graph on the right shows all training examples being classified correctly under a complex model. However, once we introduce test examples, we'll see more error in the classification.

This is commonly known as the [bias-variance tradeoff](https://en.wikipedia.org/wiki/Bias%E2%80%93variance_tradeoff) and is the source of our fitting problems.

The method of tuning our model complexity is done by regularization. More regularization results in less complex models which solves the problem of overfitting and less regularization results in more complex models which can solve the problem of underfitting. To quantify this, let's suppose we have some constant $\lambda$ that we can use for tuning. A larger value for $\lambda$ will result in more regularization, while a smaller term will result in less regularization. 

### How does one limit the complexity of a model?

The complexity of our model comes from the number of weights that are substantial enough to affect the decision boundary. In our previous examples we selected two weights ourselves which was equivalent to setting the weights of the other features to be $0$. However, if we were to include more weights we'd have to penalize large weights to limit the complexity. In other words, the loss should increase when our weights increase in magnitude. 

To do this we'll need to add an extra term to our loss function.

$$L(\mathbf{w},b)=\dfrac{1}{2m}\sum_{i=1}^m\left(y^{(i)}-h(z^{(i)})\right)^2+\dfrac{\lambda}{2m}||\mathbf{w}||^2$$

We can see that $\lambda$ is the regularization parameter that we set. The regularization term we used is $\dfrac{\lambda}{2m}||\mathbf{w}||^2$ which is using L2 regularization. Another common term is $\dfrac{\lambda}{2m}||\mathbf{w}||$ which is used in L1 regularization. 

We'll run some examples when I implement some nonlinear models such as Support Vector Machines. 

## Support Vector Machines

A support vector machine.


```python
from sklearn.svm import SVC
svm = SVC(kernel='rbf', random_state=1, gamma=0.02, C=0.1)
svm.fit(myFeatures, myClasses)
logReg1 = LogisticRegression(
    C=100000,
    solver='lbfgs',
    multi_class='ovr'
)
logReg1.fit(myFeatures, myClasses)
plot_decision_regions(
    myFeatures,
    myClasses,
    classifier=svm
)
plt.xlabel('X')
plt.ylabel('Y')
plt.show()
```


    
![png](output_40_0.png)
    



```python

```

## Decision Trees
