---
title: "Plan for Quarter 1"
date: 2021-09-07
description: Brief overview of my plan for the first quarter
categories: [
    Machine Learning,
    School
]
tags: [
    plan,
    senior experience
]
toc: false
draft: false
---

## Plan

I intend on following the Machine Learning course on [Coursera](https://www.coursera.org/learn/machine-learning/) to get a basic understanding of some of the concepts. Since the course is somewhat dated (though still quite relevant) in the ML field, I'll be using my [clone](https://github.com/texguy360/ml-coursera-python-assignments) of a [GitHub repo](https://github.com/dibgerge/ml-coursera-python-assignments) that contains the assignments in the course but written for python. 

As I advance, I'll probably want to transition into using certain libraries in python such as [Keras](https://keras.io), [TensorFlow](https://www.tensorflow.org/), and [PyTorch](https://pytorch.org/). But I'll cross that road when I get there.

There might be times that I focus on linear algebra, multivariable calculus, or other math concepts before heading forward into models that need that knowledge.

Idealy, I'd like to get two or more projects done during the school year using what I learn during my short time in class and my sporadic chunks of free time at home. So here are a couple concept ideas for safekeeping:

- Various analyses on data sets as practice (Quarter 1+2)
- Image recognition for cursive fonts (Quarter 3)
- Create a model for categorizing music by genre and *generating* music by genre. (Quarter 3+4)
- Discord bot that emulates my speech patterns in conversations (???)

## Recap of the Previous Week (8/30 - 9/3)

- Completed Week 1 of [ML Coursera](https://www.coursera.org/learn/machine-learning/home/week/1)
- Finished up to Gradient Descent in the my [python assignments](https://github.com/texguy360/ml-coursera-python-assignments)
- Deployed this blog for future project and life updates    