---
title: "Classification with Logistic Regression"
date: 2021-12-08
lastmod: 
description: 
categories: [
    Machine Learning
]
tags: [

]
draft: false
---

Over the past two months, I've continued with the online course on Coursera for Machine Learning and have covered up to the basics of neural networks. The last topic that was covered was linear regression with gradient descent, so this time we'll look at another supervised learning method which is the classification model before I dive into neural networks. Writing this, amidst other things was rather slow, but it's ready now.

As a way for me to fully comprehend these topics, these blog posts are meant to be comprehensive but also advanced. Written in the form of a guide, they're meant for me and others to come back and review concepts and gain some insights and connections that I first had while learning this. I've made a graph showing the tradeoff of complexity and clarity and what I'd like to achieve.

![Complexity vs. Clarity (for the average learner)](tradeoff.png)

For now, I'll cut down on derivations and explaining code implementations to focus on concepts. 


> Note: This will be a rather thorough rundown of classification using logistic regression. Refer back to my previous post on [Gradient Descent for Linear Regression](../gradient-descent-for-linear-regression) for any clarifications on my notation or on previous concepts.


**Key Points**:

- Understand the difference between regression and classification problems
- Understand the usefulness in the properties of the logistic curve (sigmoid function)
- Implement the logistic regression model with gradient descent

## Classification

In our previous [regression example](../gradient-descent-for-linear-regression#example-hypothesis-for-house-price), we used features for houses to predict house prices, which are continuous values (values in a spectrum). A classification problem involves using the features to classify objects into categories. This suggests that there should be a finite number of groups, whereas regression problems can produce an infinite number of results. 

### Examples
- Predict whether or not a person has a disease or not. There are two possibilities for the result: either a positive or negative result. The inputs or features that are used to predict a result can be the symptoms and the background of the patient. 

- Determine whether someone should be accepted, deferred, or denied to a college depending on their SAT and ACT scores, GPA, and class rank. 

- Classify images of handwritten numbers to the actual number. Takes in details or features of the image to make a prediction on the number based on the characteristics.




## Logistic Regression

For logistic regression, we're going to use the logistic function, also know as a sigmoid (S-shaped) function to do our classification.

The equation for the sigmoid function is 

$$S(x)= \dfrac{1}{1+e^{-x}}$$

![Sigmoid Function](sigmoid.PNG)

Although the equation may seem quite random, it's more important to note some of the important characteristics of the function.
1. As $x$ increases, the value of $S(x)$ approaches $1$.
2. As $x$ decreases, the value of $S(x)$ approaches $0$.
3. When $x=0$, the value of $S(x)$ is $0.5$.

If we have a large positive value of $x$, then our function produces a number close to $1$. If we have a large negative value of $x$, then our function produces a number close to $0$. Using this property, we can say that if the function value is close enough to $1$, we'll predict our answer to be $1$. 

The exact way to define *close enough* can be done with **decision boundaries**.

### Decision Boundaries

Since our function produces values between $0$ and $1$, we can first start off by rounding our answers. In math, we usually round down any value that has less than $0.5$ as a decimal and we round up any value that has greater than or equal to $0.5$ as a decimal. This function can be written simply as

$$\text{round}(x)=\begin{cases}0, \quad & \text{if } S(x)<0.5\newline 1, \quad & \text{if } S(x)\geq 0.5\end{cases}$$

If we graphed this function along with the sigmoid function we get

![Sigmoid Function with Rounding Prediction](sigmoid1.PNG)

Anything above the decision boundary line for the sigmoid function (in the shaded region) will predict that the category is $1$, while anything below will predict $0$.

For this decision boundary, it's quite easy to spot the $x$-values that correspond to producing the values below and above the line. When $x<0$, the value of $S(x)$ will be less than $0.5$ which suggests that the function will predict $0$. When $x\geq 0$, the value of $S(x)$ will be greather than or equal to $0.5$ which suggests that the function will predict $1$. Therefore, another way to write the decision boundary would be 

$$\text{round}(x)=\begin{cases}0, \quad & \text{if } x<0\newline 1, \quad & \text{if } x\geq 0\end{cases}$$

We can have other decision boundaries. Let's say we want to predict values of $S(x)$ that are greater than $0.95$ to fall in the $1$ category while any other values will predict $0$. We'll then have the following function:

$$\text{predict}(x)=\begin{cases}0, \quad & \text{if } S(x)\leq0.95\newline 1, \quad & \text{if } S(x)>0.95\end{cases}$$

Since $S(x)=0.95$ at around $x=2.94$, the decision boundary can also be written as

$$\text{predict}(x)=\begin{cases}0, \quad & \text{if } x\leq2.94\newline 1, \quad & \text{if } x> 2.94\end{cases}$$

![Sigmoid Function with $S(x)>0.95$ Decision Boundary](sigmoid2.PNG)

Currently, we've been manually choosing these decision boundaries. Now we'll need a hypothesis to generalize this decision boundary, and finally implement gradient descent to have the computer find the decision boundary.

### The Hypothesis

Currently, our sigmoid function only has one input, so what should the hypothesis with multiple inputs look like? Let's recall that the hypothesis for linear regression was

$$h(x)=x_0+\theta_1x_1+\theta_2x_2+\theta_3x_3+\cdots$$

Since we want to keep the properties of the sigmoid function the same, all we can do is compose a different function as an input rather than take only one single input. 
$$S(z)=\dfrac{1}{1+e^{-z}}$$
where $z$ is our new function with a value determined by all the features. 
For now, let's use 
$$z=x_0+\theta_1x_1+\theta_2x_2+\theta_3x_3+\cdots$$

Like before, 
1. As $z$ increases, the value of $S(z)$ approaches $1$.
2. As $z$ decreases, the value of $S(z)$ approaches $0$.
3. When $z=0$, the value of $S(z)$ is $0.5$.

> But why this value for $z$?

The value for $z$ currently uses all the features and parameters as a weighted sum. This, as we've seen with linear regression, is easiest to work with. However, we can just as well use other functions to determine the value for $z$, such as 

$$z=x_0+\theta_1x_1+\theta_2x_2+\theta_3x_1^2+\theta_4x_2^2$$

$$z=\theta_0x_0x_1x_2+\theta_1x_1x_2x_3+\theta_2x_2x_3x_4+\cdots$$

$$z=\theta_1x_1\sin(x_2)+\theta_2x_1^2\sin(x_3)$$

So $z$ doesn't have to be a weighted sum, but we'll stick to that form for now because of its simplicity.

## Cost Function

Once again, we'll need a cost function to determine the cost of the sigmoid hypothesis. Recall the [Cost Function for Linear Regression](../gradient-descent-for-linear-regression#cost-function), where the cost of the hypothesis function is modeled after the Mean Squared Error. 

$$\begin{aligned}\text{Hypothesis: }h(x)&=x_0+\theta_1x_1+\theta_2x_2+\cdots\newline\newline\text{Cost: }J(\theta)&=\dfrac{1}{2m}\sum_{i=0}^m(h(x)-y)^2\end{aligned}$$

For logistic regression, the cost function will be a different form. 

$$\begin{aligned}\text{Hypothesis: }h(x)&=S(z)=\dfrac{1}{1+e^{-z}}\newline\text{where }z&=x_0+\theta_1x_1+\theta_2x_2+\cdots\newline\newline\text{Cost: }J(\theta)&=\dfrac{-1}{m}\sum_{i=1}^m(1-y)\log{(h(x))}+y\log{(1-h(x))}\end{aligned}$$

This doesn't seem all that pleasing to look at so let's dissect it. 

Let's say we only have one training example, so $m=1$. That will get rid of the summation notation for now.
The actual value for the output is given as $y$. We have two possible values for $y$, which is $0$ and $1$. The predicted value is $h(x)$. Let's see what happens to the cost $J(\theta)$ when we have different values of $y$ and $h(x)$.


|For $y=0$           | For $y=1$ |
|:-------------------------:|:-------------------------:|
|$$J(\theta)=-\log{(h(x))}$$ | $$J(\theta)=-\log{(1-h(x))}$$|
|<img src="log1.PNG"> | <img src="log2.PNG"> |
|When $h(x)$ approaches $0$, the cost approaches $0$. When $h(x)$ approaches $1$, the cost approaches $\infty$. | When $h(x)$ approaches $1$, the cost approaches $0$. When $h(x)$ approaches $0$, the cost approaches $\infty$|

In short, the cost will tend to $0$ when the prediction is near the actual value, and the cost will increase when the prediction is farther from the value.

Since we may have multiple training examples, the total cost will be the average of all of these smaller costs of each individual training example.



## Gradient Descent

Finally, we're going to use gradient descent to update the parameters in $h(x)$. Similar to the gradient descent used for linear regression, we'll update the parameter $\theta_j$ by an amount dependent on the derivative of the cost function. 

Taking the derivative of our cost function will result in the following.

$$\dfrac{d}{d\theta}J(\theta)=\sum_{i=1}^m(h(x)-y)x$$

Neat! The derivative is the same form as the one for linear regression with the only difference being the difference in the hypothesis. Therefore, we can use the same method of updating as before.

$$h(x)=\dfrac{1}{1+e^{-z}}$$
$$\text{where }z=\theta_0+\theta_1x_1+\theta_2x_2+\cdots+\theta_jx_j+\cdots+\theta_kx_k$$
Recap:
- $k$ - the number of features
- $j$ - the index for a certain feature

For all $j$, starting from $j=0$ to $j=k$,
$$\theta_j:=\theta_j-\alpha\cdot\dfrac{1}{m}\sum_{i=1}^m(h(x)-y)x_j$$

Currently we can only separate our set into two different groups: one that corresponds with a prediction of $0$, and one that corresponds with a prediction of $1$. Since we only have $2$ possible outputs, this is called **binary classification**. To allow for more categories or classes as outputs, we'll look at multiclass classification later.
 



