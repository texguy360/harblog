---
title: "Leathercraft Wallet"
date: 2022-01-06
lastmod: 2022-01-09
description: A fun one-day project
categories: [
    Hobby
]
tags: [

]
draft: false
---

Here's something different that I just want to document for personal purposes. Since we had no school today, I took some time to do some more leathercrafting that I had started less than a month ago. My goal was to make a wallet with the leather pieces and tools that I had leftover.

Onwards!


I first chose my pieces of leather, where the major pieces were thicker full grain leather (with marks and scars) and smaller pieces to be made out of thinner top grain leather (with marks removed). I marked them up with a pencil on the flesh side (the furry side that isn't facing outside) to my desired sizes (8.5 mm by 21 mm for the main piece) and then used a pocket knife to cut them out. 

![](pieces.jpg)
![](marked-pieces.jpg)


After doing so, I folded it up and sketched in the design I wanted when burning the leather later. After testing the dye color, I figured that it was necessary to sketch the design in pen rather than pencil to give a bold outline.

![](folded-and-pencil.jpg)
![](inked.jpg)
![Reference Art](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/1125c7c3-34d3-41d3-ad16-a7bb77bcbf90/d4rjpao-752a9d96-de68-4223-b9c9-33d01f0db07e.jpg/v1/fill/w_806,h_991,q_70,strp/hibiscus_tattoo_by_tashitam_d4rjpao-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTEwNyIsInBhdGgiOiJcL2ZcLzExMjVjN2MzLTM0ZDMtNDFkMy1hZDE2LWE3YmI3N2JjYmY5MFwvZDRyanBhby03NTJhOWQ5Ni1kZTY4LTQyMjMtYjljOS0zM2QwMWYwZGIwN2UuanBnIiwid2lkdGgiOiI8PTkwMCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.OjX3s03REAL-dBqDG9LiKeUll2ueYIinBob8RKajuGs)

When it came to dyeing leather, I hadn't used any dye before so I wanted to test out the color before applying it to the final product. On some scrap pieces, I marked with pencil, pen, and burns and dyed it over with red dye. From my results, pencil marks were very faint after dyeing so like I mentioned before, I went over my pencil sketch with pen. Burning the leather and then applying the dye suppressed the boldness of the burn marks, which was not what I desired.

![](dye-test.jpg)
![](dyed-pieces.jpg)

Therefore, I dyed all my leather pieces in several layers of red to provide a rich, bloodlike shade of red. 

Now came the time to burn the leather to create a nice indented and burnt design. Using a soldering iron, I slowly went over the design and created the necessary markings to create a vibrant look.

![](dyed-design.jpg)
![](etched.jpg)

Before we get into stitching the pieces together, I like to glue the leather together using leathercraft cement so that I can set grooves and holes properly. Therefore, no parts will be shifting after I have all the holes in place, which makes for an easy stitching process. 

In my wallet design, one side contain a solid pocket, while the other side will have a clear film (cut out from binder paper dividers). This film will be stitched to the main leather piece with leather borders on the inside.

![](plastic-film.jpg)
![](glued-film.jpg)


It's time to prepare for the most time consuming part: Stitching! 

First, I marked in pencil where my stitch line will be located. I chose 3.5 mm from the edge to be the stitch line so I have enough room to trim, sand, and burnish the edges later. After marking these lines, I used a V-groover to create grooves where the stitch line will pass. Then, using a leather stitching spacer, I set the spacing for setting the holes. 

![](grooved.jpg)
![](hole-punched.jpg)

And to wrap up the major steps, we'll stitch together all the pieces using a saddle stitch pattern. This involves going over all stitch lines twice with the same thread and tying it off at both ends so that there are essentially two threads holding each of the connections together. Double the strength!

![Saddle Stich Pattern](saddle-stitch.png)

![](upright-holes.jpg)
![](upright-done.jpg)

At this point, the wallet has pretty much taken its whole shape. As a few last finishes, I burnished the edges by first sanding them down, applying some leather balm and beeswax, and rubbing it till it creates a dark red and rounded look. 

![](burnish.jpg)

I've also added some neatsfoot oil and a leather balm finishing to create a richer color and provide a gloss over the work. It looks pretty good with some thin coats and after drying. 

![](shallow-done.jpg)
![](stitched-insides.jpg)


Voila! This was fun for me to make. There are lots of imperfections (especially in my stitching haha), but it went well altogether. Note for next time is to choose thinner pieces of leather so stitches can be made easily. Also, thicker pieces of leather are generally not really fit for wallets. Try skiving the leather down a bit to create thinner pieces if only thicker pieces are around. Hopefully it was interesting to learn about leathercraft, and hopefully whoever received this wallet can enjoy it as my first and probably not last project working with leather.



