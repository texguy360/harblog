---
title: "Robotics City Meet"
date: 2022-02-14
lastmod: 
description: A reflection of what's happened till now.
categories: [
    Robotics
]
tags: [
    
]
draft: false
---

Two days ago our team had our city league tournament. There was a lot that went into it beforehand, and during the competition that made it an eventful experience. In short, this will be less technical than most posts, but it'll be a timeline of the two weeks leading up to the meet, along with my thoughts throughout. As with most times in our robotics team, there are quite a lot of troubles that we just can't ever steer clear. I'm glad that I've encountered them so I can learn some life lessons early on, but I'm also obviously bummed as it takes shape poorly in the field that I love.

## Preparation

We had two weeks to prepare for the city competition, where much of the hardware was already done, so much of what was left was coding and the engineering portfolio. I decided that to spice up the engineering portfolio, I'd fully CAD our robot using Toby's mecanum drive base CAD as a starting point. Then I'd try to include as much detail in the CAD so that the renders of the robot and any subsystems would be as precise and informational as possible.

Others had their own responsibilities to take care of, and I think for many of us, we pushed it off to the last few days, and even hours before the competition when we had two full weeks. I feel like we shot ourselves in the foot, but aren't admitting it.

Our season had a combination of prototypes made in CAD and some only by hand, so this was the first time this season we created a model of our collective designs. This made describing and accessing different parts of the robot much easier than showing certain hidden components on our physical robot. 

### The Portfolio

Below is our portfolio. I'd say it looks quite professionally made, but content wise, it can be revised quite a bit. The design process is rough, and not articulated spectacularly so that's something to edit later.

<embed src="portfolio.pdf" width="100%" height="800" type="application/pdf">

I had a total of 6 hours of sleep in the last two days. It didn't feel tiring since making a CAD model of our entire robot was very rewarding, along with helping write and format a professional portfolio. I pushed off making a CAD of our robot, but I found it very rewarding in doing so by now knowing every detail of our robot.


## The Meet

I'm really losing my composure this year. During practices, I'm used to soaking up all the nonsense and unproductive uses of time, or calling people out without getting too heated. But I've been getting more frustrated at meets, because that's where all my pent up frustration gets released as our robot is put on stage. I'm quite critical about myself, but most times silent about others. I think that my silence caught up to me and I could no longer handle being pushed around by other people's feelings, inactions, and lack of responsibility. Especially since the hardware was completed the earliest it has been, with enough time for drive practice and code implementation.
Here's a list of some troubling moments at the meet:

Hardware:
- Intake is horribly inconsistent and quite annoying to manipulate for drivers
- Grabber rotator servo was inconsistent and would stall randomly (may be electronic or code issue, but has yet to be debugged)
- Capping mechanism never worked in-game and was more of a show item than of any practical use
- Mecanum wheels were not attached optimally for turning the robot (noticed after the meet). This was the cause of many coding issues

Software:
- No auton prior to the meet
- An inconsistent auton was put together 30min before our first match
- Image processing camera not implemented
- Limit switch wasn't coded

Driving:
- Major penalty from turning the shipping hub
- Major penalty from improper duck placing
- Large strain on grabber from getting crushed while driving over the barriers

Outreach (judging):
- Events felt embellished from what we actually did, especially those that we've only planned
- Framed events that were done past seasons as done this season
- A general lack of genuineness

Much of this could've been prevented, and was called out many many times. I've already established my view on how this season has been going since the beginning of the season, and even checked up on everyone with a letter, but some things have been hard to see changes in. I do not feel like we fully deserved the Inspire award, and I currently don't feel like we have any chance at state if we continue on like this.



## What's Next

I'm honestly in a whirlpool of emotions. Robotics has been, and still is on my mind for so much of the day, but it has become emotionally taxing. I love to work on robotics, but being on Robophins has become less and less rewarding. Building a robot that I'm proud of has been the goal for the past few years. Especially being back in-person, I've tried to put in the effort that I couldn't last year in remote land. But, even after talking extensively with the team about our goals and expectations, I don't believe that our hearts align. 

There's a lot that caused this. It's not the first year, and since the robotics season is coming to a close soon, I think it'd be best if things were left as they are while I give some time to reflect. So why did this happen?

Rollback to two years ago in my sophomore year: turmoil occurred but the source of the problem was different. We were dissatisfied with the leadership because of how limiting and controlling it was. We held meetings in attempt to resolve our technical and personal issues, but that led to no conclusion and we found ourselves advancing to state and being carried by luck on our side. We were united by the same goals, but everyone had varying ideas so the problem was coming to a compromise on what ideas to use in the final robot. Our design and prototype process was also lacking so there was nothing to show until the end result was presented. 

Now this year, our team agreed on a team structure without a captain. This gives everyone free rein of whatever they want to do, but it also requires everyone to take care of their responsibilities promptly. We lacked a common thread to cling onto, because we all had varying goals. Some members are looking forward to make it to state, and that's all. Others are looking to have a good time with friends. 

I feel that merely advancing to state has become the standard, and at some point we have to aim higher. Two years ago, we were off from going to worlds by one match. Our robot that season was no match compared to those at state, but we were still able to stumble our way through the playoffs through luck. Going to worlds, or making a robot that we're all proud of is not out of reach for our group, and that was my goal for the season. Sadly and as the source of many of our problems, I don't think all of our goals aligned, and I have therefore established poor expectations for what our team is willing to accomplish. This has resulted in me being heated at times when others lacked motivation. 

Clearly, we didn't have a captain to bring us all together, so our mentality was not on the same page. Since most of our team is composed of seniors, the mentality of some people that was shown in practice was clearly one that wanted to get by and relieve themselves of high school robotics, while others had the mentality of making the best year possible. On paper, we might have the same goals, but we don't have the same mentality, motivation, or dedication to achieve this goal, and that's something that can't be changed easily. If people are willing to change on their own, that's great. If not, it's quite late this year, but I hope everyone can reflect for everyone's own individual improvement. 

This is a lot off my mind, and when I say I've been thinking about this for over 50% of my waking hours over the past several weeks and months, I am not exaggerating. Since this is rather secluded, I can quietly vent my feelings without holding back, and hopefully be able to look back and reflect more later. 