---
title: "Gradient Descent for Linear Regression"
date: 2021-09-30
lastmod: 2021-10-07
image: 
description: A look at the concepts and maths for gradient descent
categories: [
    Machine Learning
]
tags: [
    algorithm,
    code,
    math,
    python,
    regression
]
draft: false
---

Gradient descent is an algorithm used to optimize a model, most prominently with a linear regression model. It is used to decrease the error from using a certain model on a set of data. We'll start with some definitions, some intuitive approaches to understanding it, and then dive deeper into the math and code.

## Linear Regression

The goal of linear regression is to fit a line (linear graph) depending on one or more features (input/independent variables). This line is called a **model**. An example of how this regression would look would be

![Graph of linear regression](https://miro.medium.com/max/1280/1*LEmBCYAttxS6uI6rEyPLMQ.png)

The points represent the sample data that we want to make a model of. The line represents the predicted output values $y$ given some input values $x$. 

To start, we'll need an equation to make a prediction of what the output value is given the inputs. This will be the hypothesis or model of the situation. The hypothesis for linear regression will take the form of

$$h(x)=\theta_0+\theta_1x_1+\theta_2x_2+\cdots+\theta_jx_j+\cdots+\theta_kx_k$$

where
- $k$ is the number of features
- $\theta_j$ represents the coefficient/parameter of the $j$th feature
- $x_j$ represents the value of the $j$th feature

Let's take a look at an example to understand how this works.

### Example: Hypothesis for House Price

Suppose we want to predict the value of a house given its **size** and the **age** since it was built. The size and the age of the house are the **features** that affect the model. However, these features may not be weighted equally. In some neigborhoods, the size of the house will affect the price more than the age of the house, while in other neighborhoods the opposite is true. 

We can let the values of the "weights" of the size and age of the house be the parameters $\theta_1$ and $\theta_2$, respectively. These determine the importance of a feature in predicting the value of the house.

> What's up with $\theta_0$?

This is a constant term that does not scale any feature. It can be thought of as the "$y$-intercept" of our model. It is the base value when all features equal $0$.


To see this in action, let's say we have 
- $\theta_0=100000$, 
- $\theta_1 = 200$, 
- and $\theta_2=-5000$. 

Our hypothesis would be
$$h(x)=100000+200x_1-5000x_2$$

The values can be interpreted so that the base value $ \\$ 100000$ for a house. For each square foot of space $x_1$ that the house occupies, an additional amount of $\\$200$ is added to its value. Finally, for each year $x_2$ since the house was built, the value of the house decreases by $\\$5000$.

Now, let's say we have a house valued at $\\$650000$ occupying $2000$ square feet of space and it was built $25$ years ago. Using these values, the predicted price of the house can be calculated with our hypothesis.

$$h(x)=100000+200(2000)-5000(25)$$
$$=375000$$

The predicted value of the house is $\\$375000$! However, the actual value of the house is $\\$650000$. With our current parameters, this doesn't seem like a great model for predicting house prices...

> So how do you choose values for parameters $\theta$ to create the *best* model given a sample set?

Before we can find out how we can choose parameters to create the *best* model, we have to define what makes a model better than another, which leads us to the **cost function**.

## Cost Function

A cost function is a method of measuring the accuracy, competence, or performance of a certain model. A poor model will have a high cost, while a better model will have a lower cost. Although we don't know what this looks like exactly yet, this may sound similar to **errors**.

### Error 

The error between a predicted value $\hat{y}$ and the actual value $y$ of a training example is the difference between those values. To account for the errors of using all the training examples, we must take the sum of these errors, producing an equation of the form

$$\text{Total Error} = \sum_{i=1}^m{(\hat{y}_i-y_i)}$$

where
- $m$ is the number of observations or training examples
- $\hat{y}_i$ is the predicted output value of the model of the $i$th training example
- $y_i$ is the actual output value (the value given from test data) of the $i$th training example

$$\text{MSE}=\dfrac{1}{m}\sum_{i=1}^{m}(\hat{y}-y)^2$$

There are some key features of this 
For the cost function, $\hat{y}$ represents the hypothesis $h(x)$ evaluated for each training example. 

 the cost function $J(\theta)$ will then be

$$J(\theta)=\dfrac{1}{2m}\sum_{i=1}^m(h(x)-y_i)^2$$

Note that the function is scaled by a constant factor of $\frac{1}{2}$ which is useful when using gradient descent. 

Since the function is equal to the average value of squares, $J(\theta)$ will be always be nonnegative. As the errors of the predicted outputs decrease overall, the cost function will also decrease. This demonstrates the main property of the cost function that is critical for gradient descent.

## Gradient Descent

Gradient descent is an algorithm to optimize linear regression so that the cost of the hypothesis is minimized. Doing this aims to increase the accuracy of making future predictions by choosing different parameters after each iteration.

The ultimate goal is to minimize the cost function, so for each iteration, small changes to the parameters have to be made for the function to tend to a minimum. Therefore, for each parameter $\theta_i$ in $\vec \theta$, the value must be increased, decreased, or kept the same. A visual intuition of how this process works can help with understanding how this works. Gradient descent is similar to how a ball rolls down a hill until it reaches a crevasse or the lowest point.


![Gradient Descent Visualization (Credit: Jacopo Bertolotti, CC0, via Wikimedia Commons)](https://upload.wikimedia.org/wikipedia/commons/a/a3/Gradient_descent.gif)


### 2D Visualization

Let's do a visualization before getting into the math. For a function with one independent variable dependent on the parameters and one dependent variable, the cost function may look like this:

![Cost function](cost.png)

Now, let's suppose that our current $\theta$ values or parameter values results in a value located at

![Cost at a point](cost1.png)

To minimize the cost of the function, the parameters must shift to the left, and decrease. Now notice that if the slope of the graph at the point is found, that slope can be used to determine the direction and amount that the parameters need to be adjusted.

![Slope used to change parameters](cost2.png)

After one iteration, the cost decreases and the slope of the cost function at the new point can be calculated to further adjust the parameters.

![Cost decreased; repeat](cost3.png)

Eventually, the cost will converge to a local minimum.

### The Mathematics

At this point, we should have a basic conceptual understanding of how the process of gradient descent works. Starting with creating a model with linear regression, then determining the 

Visually, the gradient is the slope or incline of the function or surface at a certain point. As seen in the one parameter example, the change in parameter is made in the direction to minimize the cost, or opposite of incline. 

Mathematically, the term gradient represents a vector of slopes for each parameter (or in each axis direction). 

#### Two Parameters / One Feature Example

For a model with one feature $x$, the hypothesis will have two parameters. 
$$h_\theta(x)=\theta_0+\theta_1x$$

Since the gradient of the parameters is said to be a vector of the slopes for each parameter, we can find the slopes of each parameter *individually* since they are assumed to be *independent* of one another.

$$\dfrac{d}{d\theta_j}J(\theta)$$

If the slope is positive, we want to decrement the parameter. If the slope is negative, we want to increment the parameter. Therefore we can write an equation for each parameter:


$$\theta_0 \coloneqq \theta_0 - \alpha \cdot \dfrac{d}{d\theta_0}J(\theta)$$
$$\theta_1 \coloneqq \theta_1 - \alpha \cdot \dfrac{d}{d\theta_1}J(\theta)$$

where 
- `:=` represents the assignment operator
- $\alpha$ represents the learning rate (how fast to update parameters)

Now let's solve for the derivatives:

$$\begin{aligned}\dfrac{d}{d\theta_0}J(\theta)&=\dfrac{d}{d\theta_0}\left(\dfrac{1}{2m}\sum_{i=1}^m(h_\theta(x^{(i)})-y^{(i)})^2\right) \newline &=\dfrac{1}{2m}\sum_{i=1}^m\dfrac{d}{d\theta_0}(\theta_0+\theta_1x-y^{(i)})^2 \newline &=\dfrac{1}{2m}\sum_{i=1}^m 2(\theta_0+\theta_1x-y^{(i)})\dfrac{d}{d\theta_0}(\theta_0+\theta_1x-y^{(i)}) \newline &=\dfrac{1}{m}\sum_{i=1}^m (\theta_0+\theta_1x-y^{(i)}) \newline &=\dfrac{1}{m}\sum_{i=1}^m(h_\theta(x^{(i)})-y^{(i)})\end{aligned}
$$
The same can be done with the parameter for the singular feature. The result will be similar but not exactly the same:
$$\begin{aligned}\dfrac{d}{d\theta_1}J(\theta)&=\dfrac{d}{d\theta_1}\left(\dfrac{1}{2m}\sum_{i=1}^m(h_\theta(x^{(i)})-y^{(i)})^2\right) \newline &=\dfrac{1}{2m}\sum_{i=1}^m\dfrac{d}{d\theta_1}(\theta_0+\theta_1x-y^{(i)})^2 \newline &=\dfrac{1}{2m}\sum_{i=1}^m 2(\theta_0+\theta_1x-y^{(i)})\dfrac{d}{d\theta_1}(\theta_0+\theta_1x-y^{(i)}) \newline &=\dfrac{1}{m}\sum_{i=1}^m (\theta_0+\theta_1x-y^{(i)})\cdot x \newline &=\dfrac{1}{m}\sum_{i=1}^m(h_\theta(x^{(i)})-y^{(i)})x\end{aligned}
$$

Plugging these back into the assignments for the parameters results in

$$\theta_0 \coloneqq \theta_0 - \alpha \cdot \dfrac{1}{m}\sum_{i=1}^m(h_\theta(x^{(i)})-y^{(i)})$$
$$\theta_1 \coloneqq \theta_1 - \alpha \cdot \dfrac{1}{m}\sum_{i=1}^m(h_\theta(x^{(i)})-y^{(i)})x$$

Now all the work is pretty much done! For every iteration in the gradient descent algorithm, these assignments are run simultaneously on the updated parameters $\theta$. After a sufficient amount of iterating along with a properly picked learning rate, the parameter should converge as to minimize the cost function. Wonderful! 


#### Generalization for Multiple Parameters

Let's return to our hypothesis for $k$ features:

$$h_\theta(x)=\theta_0+\theta_1x_1+\theta_2x_2+\cdots+\theta_jx_j+\cdots+\theta_kx_k$$

Similarly, we need to find the gradient, or the vector of slopes of each individual parameter. In the previous section we used the regular derivative $\dfrac{d}{d\theta_j}$ and assumed that parameters were independent of one other. 

The more proper notation would be to use the partial derivative $\dfrac{\partial}{\partial\theta_j}$, which finds the derivative with respect to a variable assuming the other variables are constant (which is the same calculation in practice).


<!--
#### Using Linear Algebra (#TODO)

## Implementation in Python (#TODO) -->

## Glossary of Terminology


