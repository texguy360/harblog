---
title: "Initial Post"
date: 2021-09-05
description: Welcome! Here's the setup process for my Hugo website hosted on GitLab.
categories: [
    School
]
tags: [
    senior experience,
    website
]
draft: false
---

***

School's started, and to document my work in Senior Experience (and beyond), this'll be the start to my website. 

## Setup
I installed Hugo extended [v0.87.0](https://github.com/gohugoio/hugo/releases/tag/v0.87.0) and saved it into the Programs folder of AppData. Once that was done, I added the hugo executable to my path in the environment variables list. 

To actually create a site, I allocated a folder in which I ran the following command in the terminal:

```go
hugo new site
```

I then chose a [theme](https://themes.gohugo.io) so I didn't have to code from scratch the base html and css required for a good-looking website. You can find the theme of this website from the credits in the footer or go straight to the [repository](https://github.com/CaiJimmy/hugo-theme-stack) where there's documentation on how to install and configure the website theme. After some tinkering, I ran ```hugo server``` to deploy the server.

> Make sure the lines in the config file are edited from the template.

**Success!**

***

## Hosting on GitLab

I chose GitLab since I already have a website hosted on GitHub Pages that I didn't want to take down. Start by creating a remote repository on GitLab with the website folder name as the repo name. Then initiate the website folder as a git repo with ```git init```.

A config file designated to run a script to start up the Hugo website is necessary, and the contents of it can be found in the [docs](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/).

Then continue by creating a commit and pushing the code to GitLab. The code I used was 

```git
git add .
git commit -m "Initial commit"
git remote add origin https://gitlab.com/texguy360/harblog.git
git push -u origin master
```

Done! (after failing several times since I needed to add the theme as a Hugo module with the correct path...and make sure the theme path is added as a git submodule)

The website should have been built successfully so visiting `https://texguy360.gitlab.io/harblog` worked and I then went along to learn some of the functionalities in Hugo. I've linked some resources which I used to get through the setup process and understand how to add content to the website.



## Resources

- [Hugo YouTube Tutorial](https://youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3)
- [Hugo Official Docs](https://gohugo.io/documentation/)
- [Hugo - Hosting on GitLab](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/)

***

